<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Schema;

class DatabaseHelper
{
    /**
     * Helper to checking connection given has any table given
     *
     * @param string $table_name table name
     * @param string $connection connection name, by default use laravel default connection
     * @return boolean true if the connection has table given
     */
    public static function isTableExist(string $table_name, string $connection = 'mysql')
    {
        return Schema::connection($connection)->hasTable($table_name);
    }

    /**
     * Check dateTime value and give a null if false condition
     *
     * @param string|void $dateTime datetime value
     * @return void result after checking datetime value
     */
    public static function dateTimeValue($dateTime)
    {
        return strtotime('1970-01-01 00:00:00') > strtotime($dateTime) ? null : $dateTime;
    }
}
