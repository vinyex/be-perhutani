<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Check connection multiple databases function
     *
     * @return void
     */
    public function checkConnection()
    {
        $res = [];
        $lists = ['', 'ASME_GENERAL', 'ASME_ORGANIZATIONAL', 'ASME_REPORTS', 'ASME_SYSTEM', 'ASME_TRX'];

        for ($i = 0; $i < count($lists); $i++) {
            $arr = [];
            try {
                DB::connection($lists[$i])->getPDO();
                $arr[$lists[$i] == '' ? 'main_database' : $lists[$i]] = DB::connection($lists[$i])->getDatabaseName();
            } catch (\Exception $e) {
                $arr[$lists[$i] == '' ? 'main_database' : $lists[$i]] = false;
            }

            $res[] = $arr;
        }

        return response()->json([
            'message' => 'List DB Connection',
            'databases' => $res
        ]);
    }
}
