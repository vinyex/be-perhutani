<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('klasifikasi_masalah', function (Blueprint $table) {
            $table->id()->comment('new name column from existing: klasifikasi_masalah_id');
            $table->integer('klasifikasi_masalah_first_id')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('klasifikasi_masalah_first_to_parent_pipe', 255);
            $table->integer('klasifikasi_masalah_level')->nullable();
            $table->integer('active');
            $table->integer('entry_by')->nullable();
            $table->integer('modify_by')->nullable();
            $table->integer('ordering')->nullable();
            $table->integer('jenis_klasifikasi')->comment('new name column from existing: Jenis_klasifikasi');
            $table->string('klasifikasi_masalah_kode')->nullable();
            $table->string('klasifikasi_masalah_name', 255);
            $table->text('description')->nullable();
            $table->string('rubikcubeg', 255)->nullable();
            $table->integer('org_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('klasifikasi_masalah');
    }
};
