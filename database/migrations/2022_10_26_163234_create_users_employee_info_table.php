<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_employee_info', function (Blueprint $table) {
            $table->id()->comment('new column name from existing: user_employee_info_id');
            $table->integer('user_id')->nullable();
            $table->string('employee_id', 25)->nullable()->comment('new column name from existing: employeeId');
            $table->string('date_employment', 4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_employee_info');
    }
};
