<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grights', function (Blueprint $table) {
            $table->id()->comment('new column name from existing: gright_id');
            $table->string('name', 20)->nullable();
            $table->string('description', 100)->nullable();
            $table->integer('level')->nullable();
            $table->integer('active')->nullable();
            $table->integer('entry_by')->nullable();
            $table->integer('modify_by')->nullable();
            $table->string('rubikcubeg', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grights');
    }
};
