<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orgs_detail_info', function (Blueprint $table) {
            $table->id()->comment('new name column from existing: org_detail_info_id');
            $table->string('org_id', 50)->nullable();
            $table->string('address', 255)->nullable();
            $table->string('area_code', 20)->nullable();
            $table->string('area_name', 50)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('postal_code', 6)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orgs_detail_info');
    }
};
