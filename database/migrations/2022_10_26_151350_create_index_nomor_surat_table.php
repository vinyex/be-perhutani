<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('index_nomor_surat', function (Blueprint $table) {
            $table->id()->comment('new name column from existing: index_nomor_surat_id');
            $table->string('tahun', 255)->nullable();
            $table->integer('index')->nullable();
            $table->string('subindex_nomor_surat', 255)->nullable();
            $table->string('subindex_nomor_surat_unique', 255)->nullable()->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('index_nomor_surat');
    }
};
