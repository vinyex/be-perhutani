<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addressbook_list', function (Blueprint $table) {
            $table->id()->comment('new name column from existing: addressbook_list_id');
            $table->integer('addressbook_group_id');
            $table->integer('org_id');
            $table->integer('org_position_id');
            $table->integer('entry_by');
            $table->integer('modify_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addressbook_list');
    }
};
