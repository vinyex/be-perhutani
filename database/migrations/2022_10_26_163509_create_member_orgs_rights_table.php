<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member_orgs_rights', function (Blueprint $table) {
            $table->id()->comment('new column name from existing: membership_id');
            $table->string('user_email', 255)->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('org_id')->nullable();
            $table->integer('gright_id')->nullable();
            $table->integer('active')->nullable();
            $table->integer('entry_by')->nullable();
            $table->integer('modify_by')->nullable();
            $table->string('rubikcubeg', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member_orgs_rights');
    }
};
