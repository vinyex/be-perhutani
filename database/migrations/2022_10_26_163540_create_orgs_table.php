<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orgs', function (Blueprint $table) {
            $table->id()->comment('new column name from existing: org_id');
            $table->integer('org_first_id')->nullable();
            $table->integer('parent_id')->nullable();
            $table->string('org_first_to_parent_pipe', 100);
            $table->integer('org_level')->nullable();
            $table->integer('active');
            $table->integer('entry_by')->nullable();
            $table->integer('modify_by')->nullable();
            $table->integer('ordering')->nullable();
            $table->string('org_name', 100);
            $table->string('org_code', 50)->nullable();
            $table->text('description')->nullable();
            $table->string('rubikcubeg', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orgs');
    }
};
