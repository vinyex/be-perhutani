<?php

use App\Helpers\DatabaseHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (DatabaseHelper::isTableExist('addressbook_group', 'ASME_GENERAL')) {
            $data = DB::connection('ASME_GENERAL')->table('addressbook_group')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('addressbook_group')->insert([
                    'id' => $data[$i]->addressbook_group_id,
                    'name' => $data[$i]->name,
                    'entry_by' => $data[$i]->entry_by,
                    'modify_by' => $data[$i]->modify_by,
                    'created_at' => DatabaseHelper::dateTimeValue($data[$i]->date_create),
                    'updated_at' => DatabaseHelper::dateTimeValue($data[$i]->date_modify),
                ]);

                // Artisan::call('success', ['msg' => ' migration tabel addressbook_group']);
            }
        } else {
            // Artisan::call('error', ['msg' => ' migration tabel addressbook_group']);
        }

        if (DatabaseHelper::isTableExist('addressbook_list', 'ASME_GENERAL')) {
            $data = DB::connection('ASME_GENERAL')->table('addressbook_list')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('addressbook_list')->insert([
                    'id' => $data[$i]->addressbook_list_id,
                    'addressbook_group_id' => $data[$i]->addressbook_group_id,
                    'org_id' => $data[$i]->org_id,
                    'org_position_id' => $data[$i]->org_position_id,
                    'entry_by' => $data[$i]->entry_by,
                    'modify_by' => $data[$i]->modify_by,
                    'created_at' => DatabaseHelper::dateTimeValue($data[$i]->date_create),
                    'updated_at' => DatabaseHelper::dateTimeValue($data[$i]->date_modify),
                ]);

                // Artisan::call('success', ['msg' => ' migration tabel addressbook_list']);
            }
        } else {
            // Artisan::call('error', ['msg' => ' migration tabel addressbook_list']);
        }

        if (DatabaseHelper::isTableExist('index_nomor_surat', 'ASME_GENERAL')) {
            $data = DB::connection('ASME_GENERAL')->table('index_nomor_surat')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('index_nomor_surat')->insert([
                    'id' => $data[$i]->index_nomor_surat_id,
                    'tahun' => $data[$i]->tahun,
                    'index' => $data[$i]->index,
                    'subindex_nomor_surat' => $data[$i]->subindex_nomor_surat,
                    'subindex_nomor_surat_unique' => $data[$i]->subindex_nomor_surat_unique
                ]);

                // Artisan::call('success', ['msg' => ' migration tabel index_nomor_surat']);
            }
        } else {
            // Artisan::call('error', ['msg' => ' migration tabel index_nomor_surat']);
        }

        if (DatabaseHelper::isTableExist('klasifikasi_masalah', 'ASME_GENERAL')) {
            $data = DB::connection('ASME_GENERAL')->table('klasifikasi_masalah')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('klasifikasi_masalah')->insert([
                    'id' => $data[$i]->klasifikasi_masalah_id,
                    'klasifikasi_masalah_first_id' => $data[$i]->klasifikasi_masalah_first_id,
                    'parent_id' => $data[$i]->parent_id,
                    'klasifikasi_masalah_first_to_parent_pipe' => $data[$i]->klasifikasi_masalah_first_to_parent_pipe,
                    'klasifikasi_masalah_level' => $data[$i]->klasifikasi_masalah_level,
                    'active' => $data[$i]->active,
                    'entry_by' => $data[$i]->entry_by,
                    'modify_by' => $data[$i]->modify_by,
                    'created_at' => DatabaseHelper::dateTimeValue($data[$i]->date_create),
                    'updated_at' => DatabaseHelper::dateTimeValue($data[$i]->date_modify),
                    'ordering' => $data[$i]->ordering,
                    'jenis_klasifikasi' => $data[$i]->Jenis_klasifikasi,
                    'klasifikasi_masalah_kode' => $data[$i]->klasifikasi_masalah_kode,
                    'klasifikasi_masalah_name' => $data[$i]->klasifikasi_masalah_name,
                    'description' => $data[$i]->description,
                    'rubikcubeg' => $data[$i]->rubikcubeg,
                    'org_id' => $data[$i]->org_id,
                ]);

                // Artisan::call('success', ['msg' => ' migration tabel klasifikasi_masalah']);
            }
        } else {
            // Artisan::call('error', ['msg' => ' migration tabel klasifikasi_masalah']);
        }

        if (DatabaseHelper::isTableExist('toolsparameterize', 'ASME_GENERAL')) {
            $data = DB::connection('ASME_GENERAL')->table('toolsparameterize')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('toolsparameterize')->insert([
                    'id' => $data[$i]->toolsparameterize_id,
                    'user_id' => $data[$i]->user_id,
                    'param' => $data[$i]->param,
                    'value' => $data[$i]->value,
                    'rubikcubeg' => $data[$i]->rubikcubeg
                ]);

                // Artisan::call('success', ['msg' => ' migration tabel toolsparameterize']);
            }
        } else {
            // Artisan::call('error', ['msg' => ' migration tabel toolsparameterize']);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
