<?php

use App\Helpers\DatabaseHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (DatabaseHelper::isTableExist('orgs_detail_info', 'ASME_ORGANIZATIONAL')) {
            $data = DB::connection('ASME_ORGANIZATIONAL')->table('orgs_detail_info')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('orgs_detail_info')->insert([
                    'id' => $data[$i]->org_detail_info_id,
                    'org_id' => $data[$i]->org_id,
                    'address' => $data[$i]->address,
                    'area_code' => $data[$i]->area_code,
                    'area_name' => $data[$i]->area_name,
                    'city' => $data[$i]->city,
                    'country' => $data[$i]->country,
                    'postal_code' => $data[$i]->postal_code
                ]);
            }
        }

        if (DatabaseHelper::isTableExist('user_contact_address', 'ASME_ORGANIZATIONAL')) {
            $data = DB::connection('ASME_ORGANIZATIONAL')->table('user_contact_address')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('user_contact_address')->insert([
                    'id' => $data[$i]->user_contact_address_id,
                    'user_id' => $data[$i]->user_id,
                    'address' => $data[$i]->address,
                    'city' => $data[$i]->city,
                    'country' => $data[$i]->country,
                    'postal_code' => $data[$i]->postal_code
                ]);
            }
        }

        if (DatabaseHelper::isTableExist('users_employee_info', 'ASME_ORGANIZATIONAL')) {
            $data = DB::connection('ASME_ORGANIZATIONAL')->table('users_employee_info')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('users_employee_info')->insert([
                    'id' => $data[$i]->user_employee_info_id,
                    'user_id' => $data[$i]->user_id,
                    'employee_id' => $data[$i]->employeeId,
                    'date_employment' => $data[$i]->date_employment
                ]);
            }
        }

        if (DatabaseHelper::isTableExist('x6_grights', 'ASME_ORGANIZATIONAL')) {
            $data = DB::connection('ASME_ORGANIZATIONAL')->table('x6_grights')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('grights')->insert([
                    'id' => $data[$i]->gright_id,
                    'name' => $data[$i]->name,
                    'description' => $data[$i]->description,
                    'level' => $data[$i]->level,
                    'active' => $data[$i]->active,
                    'entry_by' => $data[$i]->entry_by,
                    'modify_by' => $data[$i]->modify_by,
                    'rubikcubeg' => $data[$i]->rubikcubeg,
                    'created_at' => DatabaseHelper::dateTimeValue($data[$i]->date_create),
                    'updated_at' => DatabaseHelper::dateTimeValue($data[$i]->date_modify),
                ]);
            }
        }

        if (DatabaseHelper::isTableExist('x6_member_orgspositionsrights', 'ASME_ORGANIZATIONAL')) {
            $data = DB::connection('ASME_ORGANIZATIONAL')->table('x6_member_orgspositionsrights')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('member_org_positions_rights')->insert([
                    'id' => $data[$i]->membership_id,
                    'user_email' => $data[$i]->user_email,
                    'user_id' => $data[$i]->user_id,
                    'org_id' => $data[$i]->org_id,
                    'org_position_id' => $data[$i]->org_position_id,
                    'gright_id' => $data[$i]->gright_id,
                    'active' => $data[$i]->active,
                    'entry_by' => $data[$i]->entry_by,
                    'modify_by' => $data[$i]->modify_by,
                    'rubikcubeg' => $data[$i]->rubikcubeg,
                    'created_at' => DatabaseHelper::dateTimeValue($data[$i]->date_create),
                    'updated_at' => DatabaseHelper::dateTimeValue($data[$i]->date_modify),
                ]);
            }
        }

        if (DatabaseHelper::isTableExist('x6_member_orgsrights', 'ASME_ORGANIZATIONAL')) {
            $data = DB::connection('ASME_ORGANIZATIONAL')->table('x6_member_orgsrights')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('member_orgs_rights')->insert([
                    'id' => $data[$i]->membership_id,
                    'user_email' => $data[$i]->user_email,
                    'user_id' => $data[$i]->user_id,
                    'org_id' => $data[$i]->org_id,
                    'gright_id' => $data[$i]->gright_id,
                    'active' => $data[$i]->active,
                    'entry_by' => $data[$i]->entry_by,
                    'modify_by' => $data[$i]->modify_by,
                    'rubikcubeg' => $data[$i]->rubikcubeg,
                    'created_at' => DatabaseHelper::dateTimeValue($data[$i]->date_create),
                    'updated_at' => DatabaseHelper::dateTimeValue($data[$i]->date_modify),
                ]);
            }
        }

        if (DatabaseHelper::isTableExist('x6_member_positionsrights', 'ASME_ORGANIZATIONAL')) {
            $data = DB::connection('ASME_ORGANIZATIONAL')->table('x6_member_positionsrights')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('member_positions_rights')->insert([
                    'id' => $data[$i]->membership_id,
                    'user_email' => $data[$i]->user_email,
                    'user_id' => $data[$i]->user_id,
                    'org_position_id' => $data[$i]->org_position_id,
                    'gright_id' => $data[$i]->gright_id,
                    'active' => $data[$i]->active,
                    'entry_by' => $data[$i]->entry_by,
                    'modify_by' => $data[$i]->modify_by,
                    'rubikcubeg' => $data[$i]->rubikcubeg,
                    'created_at' => DatabaseHelper::dateTimeValue($data[$i]->date_create),
                    'updated_at' => DatabaseHelper::dateTimeValue($data[$i]->date_modify),
                ]);
            }
        }

        if (DatabaseHelper::isTableExist('x6_orgs', 'ASME_ORGANIZATIONAL')) {
            $data = DB::connection('ASME_ORGANIZATIONAL')->table('x6_orgs')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('orgs')->insert([
                    'id' => $data[$i]->org_id,
                    'org_first_id' => $data[$i]->org_first_id,
                    'parent_id' => $data[$i]->parent_id,
                    'org_first_to_parent_pipe' => $data[$i]->org_first_to_parent_pipe,
                    'org_level' => $data[$i]->org_level,
                    'active' => $data[$i]->active,
                    'entry_by' => $data[$i]->entry_by,
                    'modify_by' => $data[$i]->modify_by,
                    'ordering' => $data[$i]->ordering,
                    'org_name' => $data[$i]->org_name,
                    'org_code' => $data[$i]->org_code,
                    'description' => $data[$i]->description,
                    'rubikcubeg' => $data[$i]->rubikcubeg,
                    'created_at' => DatabaseHelper::dateTimeValue($data[$i]->date_create),
                    'updated_at' => DatabaseHelper::dateTimeValue($data[$i]->date_modify),
                ]);
            }
        }

        if (DatabaseHelper::isTableExist('x6_orgs_positions', 'ASME_ORGANIZATIONAL')) {
            $data = DB::connection('ASME_ORGANIZATIONAL')->table('x6_orgs_positions')->get();
            for ($i = 0; $i < count($data); $i++) {
                DB::table('orgs_positions')->insert([
                    'id' => $data[$i]->org_position_id,
                    'org_id' => $data[$i]->org_id,
                    'org_position_first_id' => $data[$i]->org_position_first_id,
                    'parent_id' => $data[$i]->parent_id,
                    'org_position_first_to_parent_pipe' => $data[$i]->org_position_first_to_parent_pipe,
                    'org_position_level' => $data[$i]->org_position_level,
                    'active' => $data[$i]->active,
                    'entry_by' => $data[$i]->entry_by,
                    'modify_by' => $data[$i]->modify_by,
                    'ordering' => $data[$i]->ordering,
                    'org_position_name' => $data[$i]->org_position_name,
                    'org_position_code' => $data[$i]->org_position_code,
                    'description' => $data[$i]->description,
                    'rubikcubeg' => $data[$i]->rubikcubeg,
                    'created_at' => DatabaseHelper::dateTimeValue($data[$i]->date_create),
                    'updated_at' => DatabaseHelper::dateTimeValue($data[$i]->date_modify),
                ]);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
};
