<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toolsparameterize', function (Blueprint $table) {
            $table->id()->comment('new column name from existing: toolsparameterize_id');
            $table->integer('user_id')->nullable();
            $table->string('param')->nullable();
            $table->text('value')->nullable();
            $table->string('rubikcubeg', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toolsparameterize');
    }
};
